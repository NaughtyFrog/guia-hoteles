$(function () {
  var btn_modal_open;

  
  $('[data-toggle="tooltip"]').tooltip();


  $('.carousel').carousel({
    interval: 2500,
  });


  $('#reservar-modal').on('show.bs.modal', function (e) {
    console.log('show');

    // Cambiar color y deshabilitar el boton
    btn_modal_open = $(e.relatedTarget);
    btn_modal_open.addClass('btn-danger').attr('disabled', true);

    // Agregar nombre del hotel al título
    var recipient = btn_modal_open.data('whatever'); 
    var modal = $(this)
    modal.find('.modal-title').text('Contacta al Hotel ' + recipient);
  });


  $('#reservar-modal').on('shown.bs.modal', function (e) {
    console.log('shown');
  });


  $('#reservar-modal').on('hide.bs.modal', function (e) {
    console.log('hide');
  });


  $('#reservar-modal').on('hidden.bs.modal', function (e) {
    console.log('hidden');
    btn_modal_open.removeClass('btn-danger').attr('disabled', false);
  });
});
