module.exports = function (grunt) {
  require('time-grunt') (grunt);
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });

  grunt.initConfig({
    sass: {
      dist: {
        files: [
          {
            expand: true,
            cwd: "css",
            src: ["*.scss"],
            dest: "css",
            ext: ".css",
          },
        ],
      },
    },

    watch: {
      files: ["css/*.scss"],
      tasks: ["css"], // Nombre tarea grunt
    },

    browserSync: {
      dev: {
        bsFiles: {
          // browser files
          src: ["css/*.css", "*.html", "js/*.js"],
        },
      },
      options: {
        watchTask: true,
        server: {
          baseDir: "./", // Directorio base de nuestro servidor
        },
      },
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: "./",
          src: "images/*.{png,gif,jpeg,jpg}",
          dest: "dist/",
        }],
      },
    },

    copy: {
      // Pasa los archivos 'src' de la ubicación 'cws' a 'dest'
      html: {
        files: [{
          expand: true,
          dot: true,
          cws: './',
          src: ['*.html'],
          dest: 'dist'
        }]
      },
      fonts: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'node_modules/open-iconic/font',
          src: ['fonts/*.*'],
          dest: 'dist/'
        }]
      }
    },

    clean: {
      // Eliminar la carpeta 'src'
      build: {
        src: ['dist/']
      }
    },

    cssmin: {
      dist: {}
    },

    uglify: {
      dist: {}
    },

    filerev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        lenght: 20
      },
      release: {
        files: [{
          src: [
            'dist/js/*.js',
            'dist/css/*.css'
          ]
        }]
      }
    },

    concat: {
      options: {
        separator: ';',
      },
      dist: {}
    },

    useminPrepare: {
      foo: {
        dest: 'dist',
        src: [
          'index.html',
          'contacto.html',
          'hoteles.html',
          'nosotros.html',
          'precios.html'
        ]
      },
      options: {
        flow: {
          steps: {
            css: ['cssmin'],
            js: ['uglify']
          },
          post: {
            css: [{
              name: 'cssmin',
              createConfig: function(context, block) {
                var generated = context.options.generated;
                generated.options = {
                  keepSpecialComments: 0,
                  rebase: false
                }
              }
            }]
          }
        }
      }
    },

    usemin: {
      html: [
        'dist/index.html',
        'dist/contacto.html',
        'dist/hoteles.html',
        'dist/nosotros.html',
        'dist/precios.html'
      ],
      options: {
        assetsDir: ['dist', 'dist/css', 'dist/js']
      }
    }
  });

  // Eliminamos todos lso grunt.loadNpmTasks() porque con el requiered ya tenemos puesto el jit-grunt, así que ya no hace falta definirlos

  grunt.registerTask("css", ["sass"]);
  grunt.registerTask("default", ["browserSync", "watch"]);
  grunt.registerTask("img:compress", ["imagemin"]);
  grunt.registerTask('build', [
    'clean',
    'sass',
    'copy',
    'imagemin',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
  ])
};
